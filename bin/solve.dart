import 'dart:io';

import 'package:args/args.dart';

import 'package:hashcode_2018_qualifier/lib.dart';

main(List<String> arguments) {
  // Define argument parser and start parsing.
  var parser = new ArgParser()
    ..addOption('output', abbr: 'o')
    ..addOption('solver', abbr: 's', defaultsTo: 'Solver1')
    ..addFlag('debug', defaultsTo: false);
  var argResults = parser.parse(arguments);

  // Get and fetch name of input file.
  var input = argResults.rest.length > 0 ? argResults.rest[0] : null;
  if (input == null) {
    print('Missing input file!');
    exit(1);
  }

  // Get the name of the output file.
  var output = input + '.out';
  if (argResults['output'] != null) {
    output = argResults['output'];
  }

  // Create the solver.
  var solver = new Solver(argResults['solver'], input, output);
  solver.solve();
  solver.output();
}
