import 'dart:io';

import 'package:args/args.dart';

import 'package:hashcode_2018_qualifier/lib.dart';

main(List<String> arguments) {
  // Define argument parser and start parsing.
  var parser = new ArgParser()
    ..addFlag('debug', defaultsTo: false);
  var argResults = parser.parse(arguments);

  // Get and fetch name of data file.
  var data = argResults.rest.length > 0 ? argResults.rest[0] : null;
  if (data == null) {
    print('Missing data file!');
    exit(1);
  }

  // Get and fetch name of result file.
  var result = argResults.rest.length > 1 ? argResults.rest[1] : null;
  if (result == null) {
    print('Missing result file!');
    exit(1);
  }

  // Create the solver.
  var scorer = new Scorer(data, result);
  scorer.score();
}
