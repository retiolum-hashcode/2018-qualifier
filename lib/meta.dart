// Meta data from the data set.
class Meta {
  // Properties from the data set.
  int rows, cols, cars, rides, bonus, time;

  // Constructor (from line from the data set).
  Meta(String line) {
    List<int> props = line.split(' ').map((item) => int.parse(item)).toList();
    this.rows = props[0];
    this.cols = props[1];
    this.cars = props[2];
    this.rides = props[3];
    this.bonus = props[4];
    this.time = props[5];
  }
}
