import 'package:t_stats/t_stats.dart';

import 'solver.dart';
import 'car.dart';
import 'ride.dart';

// Solver 4: taking the closest possible ride, but with respect to possible bonus, in a round-robin way, with initial car distribution.
class Solver4 extends Solver {
  // Statistics about ride distances.
  Statistic stats;

  // Whether to do bonus search or not.
  bool optimizeForBonus = false;

  // Constructor.
  Solver4(String dataFile, String resultFile): super.internal(dataFile, resultFile) {
    this.stats = new Statistic.from(this.rides.map((ride) => ride.distance).toList(), name: 'Ride distance');
    this.optimizeForBonus = (this.meta.bonus / stats.median) > 0.8;
  }

  // Solve the problem.
  void solve() {
    // Loop while we have cars and rides.
    int carsRemaining = this.cars.length;
    int round = 0;
    int count = 0;
    while (carsRemaining > 0 && this.rides.length > 0) {
      // Adjust counters.
      round += 1;
      count = 0;

      // Loop over non-full cars.
      for (var car in this.cars.where((car) => !car.full)) {
        // Adjust counter.
        count += 1;

        // Find all possible rides for the car.
        List<Ride> possibleRides = this.rides.where((ride) {
          return car.canDrive(ride);
        }).toList();

        // Any rides possible?
        if (possibleRides.length > 0) {
          // In round 1, we distribute cars among the grid.
          if (round == 1) {
            // Sort possible rides by distance.
            possibleRides.sort((a, b) {
              return a.distance.compareTo(b.distance);
            });

            // Take a ride from the first, ... quarter of the rides.
            int quarter = (4 * (count / this.meta.cars)).ceil();
            int start = (possibleRides.length * (1 - quarter) ~/ 4).clamp(0, possibleRides.length);
            int end = (possibleRides.length * quarter ~/ 4).clamp(0, possibleRides.length);
            possibleRides = possibleRides.getRange(start, end).toList();

            // Take the first one and assign it to the ride.
            Ride ride = possibleRides.removeAt(0);
            car.addRide(ride);
            this.rides.remove(ride);
          }
          else {
            // Optimize for bonus?
            if (this.optimizeForBonus) {
              // Find all possible rides with a bonus.
              List<Ride> bonusRides = possibleRides.where((ride) {
                return car.timeSpent + car.position.distanceFrom(ride.start) <= ride.earliestStart;
              }).toList();

              // Any rides with bonus?
              if (bonusRides.length > 0) {
                possibleRides = bonusRides;
              }
            }

            // Sort rides.
            possibleRides.sort((a, b) {
              return this.compareRides(a, b, car);
            });

            // Take the first one and assign it to the ride.
            Ride ride = possibleRides.removeAt(0);
            car.addRide(ride);
            this.rides.remove(ride);
          }
        }
        else {
          car.full = true;
          carsRemaining--;
        }
      }
    }
  }

  // Compare two rides (for sorting).
  int compareRides(Ride a, Ride b, Car car) {
    // After that, continue with normal sorting.
    if (this.optimizeForBonus) {
      // If high bonus, pick short rides to get many bonus scores.
      return a.distance.compareTo(b.distance);
    }
    /*
    else if (this.meta.rows > 5000) {
      // In large cities, take long rides.
      return -1 * a.distance.compareTo(b.distance);
    }
    else if (this.meta.time < 50000) {
      // When short time is available, take the next ending ride.
      return a.latestEnd.compareTo(b.latestEnd);
    }
    else {
      // Take the nearest ride.
      return car.position.distanceFrom(a.start).compareTo(car.position.distanceFrom(b.start));
    }
    */
    //return -1 * a.distance.compareTo(b.distance);
    //return a.latestEnd.compareTo(b.latestEnd);
    return car.position.distanceFrom(a.start).compareTo(car.position.distanceFrom(b.start));
  }
}
