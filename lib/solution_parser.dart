import 'parser.dart';
import 'solution.dart';

// Parser for a solution file.
class SolutionParser extends Parser {
  // Constructor.
  SolutionParser(String input): super(input);

  // Get solutions.
  List<Solution> getSolutions() {
    if (this.lines.length == 0) {
      this.read();
    }

    return this.lines.map((line) => new Solution(line)).toList();
  }
}
