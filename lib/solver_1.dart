import 'solver.dart';
import 'car.dart';
import 'ride.dart';

// Solver 1: taking the closest possible ride.
class Solver1 extends Solver {
  // Constructor.
  Solver1(String dataFile, String resultFile): super.internal(dataFile, resultFile);

  // Solve the problem.
  void solve() {
    int carsRemaining = this.cars.length;
    while (carsRemaining > 0 && this.rides.length > 0) {
      /*
      // Pick a random, non-full car.
      this.cars.shuffle();
      Car car = this.cars.firstWhere((car) => !car.full);
      */
      // Pick the first non-full car.
      Car car = this.cars.firstWhere((car) => !car.full);

      // Find all possible rides for the car.
      List<Ride> possibleRides = this.rides.where((ride) {
        return car.canDrive(ride);
      }).toList();

      // Any rides possible?
      if (possibleRides.length > 0) {
        // Sort rides.
        possibleRides.sort((a, b) {
          return car.position.distanceFrom(a.start).compareTo(car.position.distanceFrom(b.start));
        });

        // Take the first one and assign it to the ride.
        Ride ride = possibleRides.removeAt(0);
        car.addRide(ride);
        this.rides.remove(ride);
      }
      else {
        car.full = true;
        carsRemaining--;
      }
    }
  }
}
