import 'dart:io';

import 'data_parser.dart';
import 'solution_parser.dart';
import 'meta.dart';
import 'ride.dart';
import 'solution.dart';
import 'point.dart';

// Class calculating the score of a solution.
class Scorer {
  // Name of data file.
  String dataFile;

  // Name of result file.
  String resultFile;

  // Meta data from the data set.
  Meta meta;

  // Rides from the data set.
  List<Ride> rides;

  // Solutions from the result set.
  List<Solution> solutions;

  // Constuctor.
  Scorer(String this.dataFile, String this.resultFile) {
    // Read lines of the data file and parse it.
    DataParser dataParser = new DataParser(this.dataFile);
    this.meta = dataParser.getMeta();
    this.rides = dataParser.getRides();

    // Read lines of the result file and parse it.
    SolutionParser solutionParser = new SolutionParser(this.resultFile);
    this.solutions = solutionParser.getSolutions();
  }

  // Calculate the score of the result.
  void score() {
    // Validate the solution first.
    if (!this.validate()) {
      print('Solution is not valid: difference between number of rides claimed and number of rides assigned');
      exit(1);
    }

    // Calculate the score.
    Map<String, int> result = {
      'score': 0,
      'rides': this.meta.rides,
      'assignedRides': 0,
      'scoringRides': 0,
      'bonusRides': 0,
    };
    this.solutions.forEach((solution) {
      int scoreForSolution = 0;
      int time = 0;
      bool applyBonus = false;
      Point current = new Point.origin();
      result['assignedRides'] += solution.rides.length;
      while (solution.rides.length > 0) {
        // Bonus applicalble?
        applyBonus = false;

        // Fetch the next ride.
        Ride ride = this.rides.elementAt(solution.rides.removeFirst());

        // Ride already taken?
        if (ride.taken) {
          print('Solution is not valid: ride assigned multiple times');
          exit(1);
        }
        ride.taken = true;

        // Go to start of ride.
        time += current.moveTo(ride.start);

        // Wait until start?
        if (time < ride.earliestStart) {
          time = ride.earliestStart;
        }

        // Mark ride for bonus?
        if (time == ride.earliestStart) {
          applyBonus = true;
        }

        // Ride!
        time += current.moveTo(ride.end);

        // Ride in time?
        if (time <= ride.latestEnd) {
          result['scoringRides']++;
          scoreForSolution += ride.distance;
          if (applyBonus) {
            result['bonusRides']++;
            scoreForSolution += this.meta.bonus;
          }
        }

        // Time out?
        if (time >= this.meta.time) {
          break;
        }
      }

      // Add up score.
      result['score'] += scoreForSolution;
    });

    // Make some output.
    print('Rides: ${result['rides']}');
    print('Assigned rides: ${result['assignedRides']}');
    print('Rides scoring: ${result['scoringRides']}');
    print('Rides with bonus: ${result['bonusRides']}');
    print('Score: ${result['score']}');
  }

  // Validate the solutions (for now only checking the number of assignments).
  // @todo Check that we don't have duplicate assignments.
  bool validate() {
    return this.solutions.fold(true, (carry, solution) {
      return carry && solution.valid;
    });
  }
}
