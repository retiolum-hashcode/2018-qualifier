import 'dart:io';

import 'data_parser.dart';
import 'meta.dart';
import 'ride.dart';
import 'car.dart';

import 'solver_1.dart';
import 'solver_2.dart';
import 'solver_3.dart';
import 'solver_4.dart';

// Abstract base class for all solvers.
abstract class Solver {
  // Name of data file.
  String dataFile;

  // Name of result file.
  String resultFile;

  // Meta data from the data set.
  Meta meta;

  // Rides from the data set.
  List<Ride> rides;

  // Vehicles from the data set.
  List<Car> cars;

  // Constructor.
  Solver.internal(String this.dataFile, String this.resultFile) {
    // Read lines of the input file and parse it.
    DataParser parser = new DataParser(this.dataFile);
    this.meta = parser.getMeta();
    this.rides = parser.getRides();
    this.cars = parser.getCars();
  }

  // Factory constructor.
  factory Solver(String name, String dataFile, String resultFile) {
    switch (name) {
      case 'Solver1':
        return new Solver1(dataFile, resultFile);

      case 'Solver2':
        return new Solver2(dataFile, resultFile);

      case 'Solver3':
        return new Solver3(dataFile, resultFile);

      case 'Solver4':
        return new Solver4(dataFile, resultFile);

      default:
        print('Unknown solver!');
        exit(1);
    }
  }

  // Solve the problem.
  void solve();

  // Create the output.
  void output() {
    var file = new File(this.resultFile);
    var sink = file.openWrite();
    for (var car in this.cars) {
      sink.write('${car.rides.length}');
      for (var ride in car.rides) {
        sink.write(' ${ride.index}');
      }
      sink.write('\n');
    }
    sink.close();
  }
}
