import 'point.dart';

// A ride wit all of its data.
class Ride {
  // Properties from the data set.
  int index;
  Point start, end;
  int earliestStart, latestEnd;

  // Calculated properties.
  int latestStart, earliestEnd, distance;
  bool taken = false;

  // Constructor (from line from the data set).
  Ride(String line, int this.index) {
    // Set properties from data set.
    List<int> props = line.split(' ').map((item) => int.parse(item)).toList();
    this.start = new Point(props[0], props[1]);
    this.end = new Point(props[2], props[3]);
    this.earliestStart = props[4];
    this.latestEnd = props[5];

    // Calculate some more properties.
    this.latestStart = this.latestEnd - this.start.distanceFrom(this.end);
    this.earliestEnd = this.earliestStart + this.start.distanceFrom(this.end);
    this.distance = this.start.distanceFrom(this.end);
  }
}
