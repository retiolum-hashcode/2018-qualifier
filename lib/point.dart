// A point in the city grid.
class Point {
  // Coordinates.
  int x, y;

  // Constructor.
  Point(int this.x, int this.y);

  // Constructor (for origin).
  Point.origin(): this(0, 0);

  // Get distance between points.
  int distanceFrom(Point p) {
    return (this.x - p.x).abs() + (this.y - p.y).abs();
  }

  // Move point to some other point (returning the distance).
  int moveTo(Point p) {
    int distance = this.distanceFrom(p);
    this.x = p.x;
    this.y = p.y;

    return distance;
  }
}
