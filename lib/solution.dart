import 'dart:collection';

// A single solution (i.e. the assignment of rides to a single car).
class Solution {
  // List of rides assigned to the car (for scoring).
  Queue<int> rides;

  // Number of rides (as indicated by the result file).
  int numRides;

  // Is the solution valid (only in terms of counting rides)?
  bool valid = true;

  // Constructor (from line of result).
  Solution(String line) {
    this.rides = new Queue.from(line.split(' ').map((item) => int.parse(item)));
    this.numRides = this.rides.removeFirst();

    if (this.numRides != this.rides.length) {
      this.valid = false;
    }
  }
}
