import 'parser.dart';

import 'meta.dart';
import 'ride.dart';
import 'car.dart';

// Parser for the data (i.e. input file).
class DataParser extends Parser {
  // Constructor.
  DataParser(String input): super(input);

  // Get the meta data.
  Meta getMeta() {
    if (this.lines.length == 0) {
      this.read();
    }

    return new Meta(this.lines[0]);
  }

  // Get the list of rides.
  List<Ride> getRides() {
    if (this.lines.length == 0) {
      this.read();
    }

    int index = 0;

    return this.lines
      .getRange(1, this.lines.length)
      .map((line) => new Ride(line, index++))
      .toList();
  }

  // Get the list of cars.
  List<Car> getCars() {
    var meta = this.getMeta();

    List<Car> cars = <Car>[];
    for (int i = 0; i < meta.cars; i++) {
      cars.add(new Car(meta.time));
    }

    return cars;
  }
}
