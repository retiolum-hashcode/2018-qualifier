import 'dart:io';

// Abstract base for all parsers.
abstract class Parser {
  // Name of input file.
  String input;

  // Lines of the input file.
  List<String> lines = <String>[];

  // Constructor.
  Parser(String this.input);

  // Read the input file into lines.
  // @todo Convert to using the Stream API?
  void read() {
    if (FileSystemEntity.isFileSync(this.input)) {
      var file = new File(this.input);
      this.lines = file.readAsLinesSync();
    }
    else {
      print('File does not exist!');
      exit(1);
    }
  }
}
