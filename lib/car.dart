import 'dart:collection';
import 'dart:math';

import 'ride.dart';
import 'point.dart';

// A car doing rides.
class Car {
  // List of rides assigned to the car.
  Queue<Ride> rides = new Queue<Ride>();

  // Point the car is currently at.
  Point position = new Point.origin();

  // Time total and time already spent.
  int time;
  int timeSpent = 0;

  // Marker that the car can't be assigned any more rides.
  bool full = false;

  // Internal index.
  int index;

  // Random number generator (for indexing).
  static final Random rand = new Random();

  // Constructor.
  Car(int this.time) {
    this.index = Car.rand.nextInt(this.time);
  }

  // Add a ride to the car.
  void addRide(Ride ride) {
    // Add the ride to our list.
    this.rides.add(ride);

    // Mark the ride as taken.
    ride.taken = true;

    // Sum up the time spent for driving this ride.
    this.timeSpent += this.position.moveTo(ride.start);
    if (this.timeSpent < ride.earliestStart) {
      this.timeSpent = ride.earliestStart;
    }
    this.timeSpent += this.position.moveTo(ride.end);
  }

  // Can the car make a ride?
  bool canDrive(Ride ride) {
    // Time for the ride already over?
    if (this.timeSpent > ride.latestStart) {
      return false;
    }

    // Enough time left to start the ride?
    if (this.timeSpent + this.position.distanceFrom(ride.start) > ride.latestStart) {
      return false;
    }

    return true;
  }

  // Remaining time.
  int timeRemaining() {
    return this.time - this.timeSpent;
  }
}
