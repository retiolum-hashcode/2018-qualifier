import 'package:t_stats/t_stats.dart';

import 'solver.dart';
import 'car.dart';
import 'ride.dart';

// Solver 2: taking the closest possible ride, but with respect to possible bonus.
class Solver2 extends Solver {
  // Statistics about ride distances.
  Statistic stats;

  // Whether to do bonus search or not.
  bool optimizeForBonus = false;

  // Constructor.
  Solver2(String dataFile, String resultFile): super.internal(dataFile, resultFile) {
    this.stats = new Statistic.from(this.rides.map((ride) => ride.distance).toList(), name: 'Ride distance');
    this.optimizeForBonus = (this.meta.bonus / stats.median) > 0.8;
  }

  // Solve the problem.
  void solve() {
    // Loop while we have cars and rides.
    int carsRemaining = this.cars.length;
    while (carsRemaining > 0 && this.rides.length > 0) {
      // Pick the first non-full car.
      Car car = this.cars.firstWhere((car) => !car.full);

      // Find all possible rides for the car.
      List<Ride> possibleRides = this.rides.where((ride) {
        return car.canDrive(ride);
      }).toList();

      // Any rides possible?
      if (possibleRides.length > 0) {
        // Optimize for bonus?
        if (this.optimizeForBonus) {
          // Find all possible rides with a bonus.
          List<Ride> bonusRides = possibleRides.where((ride) {
            return car.timeSpent + car.position.distanceFrom(ride.start) <= ride.earliestStart;
          }).toList();

          // Any rides with bonus?
          if (bonusRides.length > 0) {
            possibleRides = bonusRides;
          }
        }

        // Sort rides.
        possibleRides.sort((a, b) {
          return compareRides(a, b, car);
        });

        // Take the first one and assign it to the ride.
        Ride ride = possibleRides.removeAt(0);
        car.addRide(ride);
        this.rides.remove(ride);
      }
      else {
        car.full = true;
        carsRemaining--;
      }
    }
  }

  // Compare two rides (for sorting).
  int compareRides(Ride a, Ride b, Car car) {
    if (this.optimizeForBonus) {
      // If high bonus, pick short rides to get many bonus scores.
      return a.distance.compareTo(b.distance);
    }
    else if (this.meta.rows > 5000) {
      // In large cities, take long rides.
      return -1 * a.distance.compareTo(b.distance);
    }
    else if (this.meta.time < 50000) {
      // When short time is available, take the next ending ride.
      return a.latestEnd.compareTo(b.latestEnd);
    }
    else {
      // Take the nearest ride.
      return car.position.distanceFrom(a.start).compareTo(car.position.distanceFrom(b.start));
    }
  }
}
